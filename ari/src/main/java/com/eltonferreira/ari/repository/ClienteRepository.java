package com.eltonferreira.ari.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eltonferreira.ari.domain.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

	public Optional<Cliente> findByCpf(String cpf);

}
